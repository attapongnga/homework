package day1;

import java.util.Scanner;


public class Lab7_2 {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int number;

		System.out.println("\tDetermine odd/even program");

		do {
			System.out.print("Enter odd number to exit loop: ");
			number = reader.nextInt();

			if (number % 2 == 0) {
				System.out.println("You entered " + number + ", it's even.");
			} else {
				System.out.println("You entered " + number + ", it's odd.");
			}

		} while (number % 2 == 0);

		System.out.println("Exited loop.");

	}
}
