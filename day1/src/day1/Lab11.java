package day1;

public class Lab11 {

	public static void main(String[] args) {
		Calculator();

		int x = 2;
		int y = 3;
		System.out.println("x + y = " + sum(x, y));
		System.out.println("10 + 20 = " + sum(10, 20));

	}

	private static void Calculator() {
		System.out.println("Calculator");
	}

	private static int sum(int a, int b) {
		return a + b;
	}
}
