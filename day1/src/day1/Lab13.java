package day1;

public class Lab13 {

	public static void main(String[] args) {
		int[][] twoD_Array = { { 1, 2 }, { 3, 4 } };

		for (int[] row : twoD_Array) {
			for (int element : row) {
				System.out.println(element);
			}
		}

		int num2 = twoD_Array[0][1];
		int num4 = twoD_Array[1][1];
		int sum2 = num2 + num4;
		System.out.println(" Array [0][1] = " + twoD_Array[0][1]);
		System.out.println(" Array [1][1] = " + twoD_Array[1][1]);
		System.out.println(" Sum = " + sum2);

	}

}
